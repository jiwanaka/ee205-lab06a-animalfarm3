#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
#include <algorithm>
#include <array>
#include <iterator>
#include <list>

#include "boolean.hpp"
#include "random.hpp"
#include "animal-factory.hpp"
#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"


using namespace std;

namespace animalfarm {

    Animal* AnimalFactory::getRandomAnimal(){
        Animal* newAnimal = NULL;
        int i = rand() % 6;
        int f = i;
        
        //this debug below shows that this function is being run multiple times when un-commenting it so I do not know why the nay, nay is overriding it when speaking 
        //cout << " " << i;
        switch(f) {
            case 0: newAnimal = new Cat (Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender());
            case 1: newAnimal = new Dog (Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender());
            case 2: newAnimal = new Nunu (Animal::getRandomBool(), RED, Animal::getRandomGender());
            case 3: newAnimal = new Aku (Animal::getRandomWeight(), SILVER, Animal::getRandomGender());
            case 4: newAnimal = new Palila (Animal::getRandomName(), YELLOW, Animal::getRandomGender());
            case 5: newAnimal = new Nene (Animal::getRandomName(), BROWN, Animal::getRandomGender());
            default: NULL;
        }
            return newAnimal;
        };
    }