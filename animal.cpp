///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>

#include "animal.hpp"

using namespace std;

namespace animalfarm {
	
void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}


string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};
	

string Animal::colorName (enum Color color) {
	/// @todo Implement this based on genderName and your work
	///       on Animal Farm 1
   switch(color){
      case 0:
         return string("Black"); break;
      case 1:
         return string("White"); break;
      case 2:
         return string("Red"); break;
      case 3:
         return string("Blue"); break;
      case 4:
         return string("Green"); break;
      case 5:
         return string("Pink"); break;
      case 6:
         return string("Silver"); break;
      case 7:
         return string("Yellow"); break;
      case 8:
         return string("Brown"); break;
      default:
         return string("Invalid Color"); break;
   }
   return NULL;
   };

 const string Animal::getRandomName() {
    int length = 9 - (rand() % 5);
    string tmp_s;
    static const char alphanum[] =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
     static const char lower_case[] =
      "abcdefghijklmnopqrstuvwxyz";   
    
   

    tmp_s.reserve(length);
tmp_s += alphanum[rand() % (sizeof(alphanum) - 1)];
    for (int i = 1; i < length; ++i) 
        tmp_s += lower_case[rand() % (sizeof(lower_case) - 1)];
    
    
    return tmp_s;
    
};

const Gender Animal::getRandomGender() {
       Gender myGender;

    myGender = Gender(rand() % 3);

    return myGender;
};

const Color Animal::getRandomColor() {
      Color myColor;

    myColor = Color(rand() % 8);

    return myColor;
}

const bool Animal::getRandomBool() {
   return rand() > (RAND_MAX / 2);
}

 /*const string Animal:: getRandomColor(void){
    Color myColor;

    myColor = Color(rand() % 8);
   
     switch(myColor){
      case 0:
         return string("Black"); break;
      case 1:
         return string("White"); break;
      case 2:
         return string("Red"); break;
      case 3:
         return string("Blue"); break;
      case 4:
         return string("Green"); break;
      case 5:
         return string("Pink"); break;
      case 6:
         return string("Silver"); break;
      case 7:
         return string("Yellow"); break;
      case 8:
         return string("Brown"); break;
      default:
         return string("Invalid Color"); break;
   }
   return NULL;

};*/

/*const string Animal:: getRandomGender(void){
    Gender myGender;

    myGender = Gender(rand() % 3);
   
   switch (myGender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};

const char* Animal::getRandomBool(){
   bool b;
   b = rand() > (RAND_MAX / 2);
return b ? "true" : "false";
}; */

const float Animal::getRandomWeight(){
   float weight = 50.0 - (rand() % 40);

   return weight;
}; 
	

	
} // namespace animalfarm
