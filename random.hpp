#pragma once
#include <string>

enum Color {BLACK, WHITE, RED, BLUE, GREEN, PINK, SILVER, YELLOW, BROWN};  /// @todo Add more colors'

enum Gender { MALE, FEMALE, UNKNOWN };

std::string randomName(const int length);
enum Color randomColor(void);
enum Gender randomGender(void);