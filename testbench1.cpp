#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

enum Names {
    NAMES_FRED,
    NAMES_GEORGE,
    NAMES_TIM,
    //...
    NAMES_COUNT, //= number of names...
};

const char* names[] = {
    "Fred",
    "George",
    "Tim"
    //...
    };


int main() {
    srand(time(0));
    Names myName;
    
    myName = Names(rand() % NAMES_COUNT);
    
    cout << "My name is, " << names[myName] << endl;

    return 0;
}