#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
#include "random.hpp"


using namespace std;

std::string randomName(const int length) {
    
    string tmp_s;
    static const char alphanum[] =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";
    
   

    tmp_s.reserve(length);

    for (int i = 0; i < length; ++i) 
        tmp_s += alphanum[rand() % (sizeof(alphanum) - 1)];
    
    
    return tmp_s;
    
}

enum Color randomColor(){
    Color myColor;

    myColor = Color(rand() % 8);

    return myColor;
}

enum Gender randomGender(){
    Gender myGender;

    myGender = Gender(rand() % 3);

    return myGender;

}
