#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
#include "boolean.hpp"
#include "random.hpp"
#include "animal.hpp"


using namespace std;

const char* colors[] = {"BLACK", "WHITE", "RED", "BLUE", "GREEN", "PINK", "SILVER", "YELLOW", "BROWN"};
const char* genders[] = {"MALE", "FEMALE", "UNKNOWN"};

int main(int argc, const char * argv[]) {
     srand( (unsigned) time(NULL) );
    

    cout << randomName(12) << endl;

    cout << colors[randomColor()] << endl;

    cout << genders[randomGender()] << endl;
    
    return 0;
}