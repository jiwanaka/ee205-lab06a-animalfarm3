#pragma once

using namespace std;

#include <string>
#include <cstdlib>
#include <ctime>

#include "animal.hpp"

namespace animalfarm{

    class AnimalFactory : public Animal{
        public:
            static Animal* getRandomAnimal();
    };
}