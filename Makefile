###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 05a - Animal Farm 2
#
# @file    Makefile
# @version 1.0
#
# @author @todo yourName <@todo yourMail@hawaii.edu>
# @brief  Lab 05a - Animal Farm 2 - EE 205 - Spr 2021
# @date   @todo dd_mmm_yyyy
###############################################################################

all: main

main2.o:  animal.hpp main2.cpp
	g++ -c main2.cpp

animal.o: animal.hpp animal.cpp
	g++ -c animal.cpp
	
mammal.o: mammal.hpp mammal.cpp
	g++ -c mammal.cpp

boolean.o: boolean.hpp boolean.cpp
	g++ -c boolean.cpp

cat.o: cat.cpp cat.hpp
	g++ -c cat.cpp

dog.o: dog.cpp dog.hpp
	g++ -c dog.cpp

fish.o: fish.hpp fish.cpp
	g++ -c fish.cpp

nunu.o: nunu.cpp nunu.hpp
	g++ -c nunu.cpp

aku.o: aku.cpp aku.hpp
	g++ -c aku.cpp

bird.o: bird.hpp bird.cpp
	g++ -c bird.cpp

palila.o: palila.cpp palila.hpp
	g++ -c palila.cpp

nene.o: nene.cpp nene.hpp
	g++ -c nene.cpp

animal-factory.o: animal-factory.cpp animal-factory.hpp
	g++ -c animal-factory.cpp

main: main2.cpp *.hpp main2.o animal.o mammal.o cat.o dog.o fish.o nunu.o aku.o boolean.o bird.o palila.o nene.o animal-factory.o
	g++ -o main main2.o animal.o mammal.o cat.o dog.o fish.o nunu.o aku.o boolean.o bird.o palila.o nene.o animal-factory.o
	
clean:
	rm -f *.o main