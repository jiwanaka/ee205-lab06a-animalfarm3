#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
#include <algorithm>
#include <array>
#include <iterator>
#include <list>
#include "boolean.hpp"
#include "random.hpp"
#include "animal.hpp"
#include "animal-factory.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"


using namespace std;
using namespace animalfarm;

int main() {


    srand( (unsigned) time(NULL) );

        //cout << Animal::getRandomName() << endl;

        //cout << Animal::getRandomWeight() << endl;

       // cout << Animal::getRandomColor() << endl;

       // cout << Animal::getRandomGender() << endl;

       // cout << Animal::getRandomBool() << endl;
array<Animal*, 30> animalArray;
animalArray.fill(NULL);
list<Animal*> animalList;

/*for( int i = 0 ; i < 25 ; i++ ) {
		a[i] = NULL;
	}*/
 
    cout << "Array of Animals: " << endl;
    cout << "  Is it empty:  " << BoolToString(animalArray.empty()) << endl;
    cout << "  Number of elements:  " << animalArray.size() << endl;
    cout << "  Max size:  " << animalArray.max_size() << endl;

           for (int i = 0; i<25 ; i++){
            
            animalArray[i] = AnimalFactory::getRandomAnimal();
            cout << animalArray[i]->speak() << endl;
            //cout << a[i]->printInfo() << endl;
        }

    for (int i = 0; i < 25; i++){
        animalList.push_front(AnimalFactory::getRandomAnimal());
    }

        cout << "Array of Animals: " << endl;
    cout << "  Is it empty:  " << BoolToString(animalList.empty()) << endl;
    cout << "  Number of elements:  " << animalList.size() << endl;
    cout << "  Max size:  " << animalList.max_size() << endl;

    for (auto const &v : animalList){
        cout << v->speak() << endl;
    }

}